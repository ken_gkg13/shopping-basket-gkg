 
 $( document ).ready(function() {
 $(".btn-remove-item").click(function(){
   		var itemName = $(this).attr('data-name');
   		$('.progressBackgroundFilter').attr('style', 'display:block');
   		var basketId = $('#lblid').html();
   		var basketNum = $('#lblnum').html();
   		var basketName =$('#lblname').html();
   		
   		localStorage.setItem("basketId",basketId);
  		localStorage.setItem("basketName", basketName);
  		localStorage.setItem("numberOfItems", basketNum);
  		
   		var obj =JSON.stringify({
            itemName: itemName
        })
   		 $.ajax({
            type: "POST",
            url: "/api/v1/baskets/removeToBasket/" + basketId,
            data: obj,
            contentType: "application/json",
        	dataType: "json",
            success: function (data) {
                if (data == 'false') {
                    return;
                }
                else {
            		numItems = localStorage.getItem("numberOfItems");
            		numItems = parseInt(numItems) -1;
            		localStorage.setItem("numberOfItems",numItems);
                    location.reload();
                    return;
                }

            },
            error: function(xhr,msg,err) { 
				//alert( msg + ', ' + err);
				
				$('.progressBackgroundFilter').attr('style', 'display:none');
					$('#lblERROR').attr('style', 'color:#b83f3f;display:block');
				if(xhr.status == 400){
					var resp =JSON.parse(xhr.responseText);
					alert(resp.message);
				}else{
					var resp =JSON.parse(xhr.responseText);
					alert(resp.message);
				}
                return;
            }   

        });

   });
   
   
    $(".btn-add-item").click(function(){
   		var itemName = $(this).attr('data-name');
   		$('.progressBackgroundFilter').attr('style', 'display:block');
   		var basketId = $('#lblid').html();
   		var basketNum = $('#lblnum').html();
   		var basketName =$('#lblname').html();
   		
   		localStorage.setItem("basketId",basketId);
  		localStorage.setItem("basketName", basketName);
  		localStorage.setItem("numberOfItems", basketNum);
  		
   		var obj =JSON.stringify({
            itemName: itemName
        })
   		 $.ajax({
            type: "POST",
            url: "/api/v1/baskets/addToBasket/" + basketId,
            data: obj,
            contentType: "application/json",
        	dataType: "json",
            success: function (data) {
                if (data == 'false') {
                    return;
                }
                else {
            		numItems = localStorage.getItem("numberOfItems");
            		numItems = parseInt(numItems) +1;
            		localStorage.setItem("numberOfItems",numItems);
                    location.reload();
                    return;
                }

            },
            error: function(xhr,msg,err) { 
				//alert( msg + ', ' + err);
				
				$('.progressBackgroundFilter').attr('style', 'display:none');
					$('#lblERROR').attr('style', 'color:#b83f3f;display:block');
				if(xhr.status == 400){
					var resp =JSON.parse(xhr.responseText);
					$('#lblERROR').html(resp.message);
				}else{
					$('#lblERROR').html('An unexpected error occured.');
				}
                return;
            }   

        });
  
   });
   
 });