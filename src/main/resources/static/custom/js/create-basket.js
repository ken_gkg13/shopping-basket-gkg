$( document ).ready(function() {
   var customer = localStorage.getItem("customerId");
   if(customer == undefined){
   	window.location.href = "/login";
   }
 });
 
 
 
 
 $('#btnCREATE').click(function () {
 	var customer = localStorage.getItem("customerId");
    $('.progressBackgroundFilter').attr('style', 'display:block');
    var basket_name, description;
    basket_name = $("#txtBASKETNAME").val();
    description = $("#txtBASKETDESCRIPTION").val();

    var valid = true;
    if (basket_name == '') {

        $("#txtBASKETNAME").addClass('input-error');
        valid = false;
    } else {
        $("#txtBASKETNAME").removeClass('input-error');
    }

    
    if (valid == true) {
		$('#lblERROR').attr('style', 'color: #b83f3f;display:none');
        var obj =JSON.stringify({
            basketDescription: description,
            basketName: basket_name,
            status : 'Active',
            userId : customer
        })
        $.ajax({
            type: "POST",
            url: "/api/v1/baskets/create",
            data: obj,
            contentType: "application/json",
        	dataType: "json",
            success: function (data) {
                $('.progressBackgroundFilter').attr('style', 'display:none');
                if (data == 'false') {
                    $('#lblERROR').attr('style', 'color: #b83f3f;display:block');
					$('#lblERROR').html('Invalid username or password.');
                    return;
                }
                else {
                    $('#lblERROR').attr('style', 'color: #b83f3f;display:none');
                    window.location.href = "/my-baskets";
                    return;
                }

            },
            error: function(xhr,msg,err) { 
				//alert( msg + ', ' + err);
				
				$('.progressBackgroundFilter').attr('style', 'display:none');
					$('#lblERROR').attr('style', 'color:#b83f3f;display:block');
				if(xhr.status == 400){
					var resp =JSON.parse(xhr.responseText);
					$('#lblERROR').html(resp.message);
				}else{
					$('#lblERROR').html('An unexpected error occured.');
				}
                return;
            }   

        });

    }

    else {
        $('.progressBackgroundFilter').attr('style', 'display:none');
        $('#lblERROR').attr('style', 'color: #E44D4D;display:block');
		$('#lblERROR').html('Please enter required fields.');
    }


});



