$( document ).ready(function() {

   var customer = localStorage.getItem("customerId");
   if(customer == undefined){
   	window.location.href = "/login";
   }
   
  $.get("/api/v1/baskets/summary/"+customer, 
  function(data, status){
  	var resp_data =JSON.stringify(data);
  	
  	for(var x=0;x<data.basketList.length;x++){
  
  	 	$("#divBasketsContainer").append('<div class="col-3 item-box"><div class="item-details"><div class="item-details"><label><a href="/view-basket/' + 
  	 	data.basketList[x].basketId + '">' + 
  	 	data.basketList[x].basketName + '</a><span class="price">| PHP ' + data.basketList[x].basketTotalPrice.toFixed(2) + '</span></label> <br><p>'
  	 	 + data.basketList[x].numberOfItems + ' items in basket</p></div></div>');

  	}
  	

   $("#lbltotalBaskets").html(data.numberofBaskets);
    $("#lbltotalPrice").html(data.basketTotalPrice.toFixed(2));
  });
   
   
   
});

