$('#btnSIGNUP').click(function () {
    $('.progressBackgroundFilter').attr('style', 'display:block');
    var user_name, password, last_name, first_name;
    user_name = $("#txtUSERNAME").val();
    password = $("#txtPASSWORD").val();
	last_name = $("#txtLASTNAME").val();
	first_name = $("#txtFIRSTNAME").val();
	
    var valid = true;
    if (user_name == '') {

        $("#txtUSERNAME").addClass('input-error');
        valid = false;
    } else {
        $("#txtUSERNAME").removeClass('input-error');
    }

    if (password == '') {

        $("#txtPASSWORD").addClass('input-error');
        valid = false;
    } else {
        $("#txtPASSWORD").removeClass('input-error');
    }


 	if (last_name == '') {

        $("#txtLASTNAME").addClass('input-error');
        valid = false;
    } else {
        $("#txtLASTNAME").removeClass('input-error');
    }


    if (first_name == '') {

        $("#txtFIRSTNAME").addClass('input-error');
        valid = false;
    } else {
        $("#txtFIRSTNAME").removeClass('input-error');
    }
    
    if (valid == true) {
		$('#lblERROR').attr('style', 'color: #b83f3f;display:none');
        var obj =JSON.stringify({
            email: user_name,
            password: password,
            firstName: first_name,
            lastName: last_name,
            status: 'Active'
        })
        $.ajax({
            type: "POST",
            url: "/api/v1/users/create",
            data: obj,
            contentType: "application/json",
        	dataType: "json",
            success: function (data) {
                $('.progressBackgroundFilter').attr('style', 'display:none');
                if (data == 'false') {
                    $('#lblERROR').attr('style', 'color: #b83f3f;display:block');
					$('#lblERROR').html('Invalid username or password.');
                    return;
                }
                else {
                    $('#lblERROR').attr('style', 'color: #b83f3f;display:none');
                    localStorage.setItem("customerId", data.userId);
                    window.location.href = "/login";
                    return;
                }

            },
            error: function(xhr,msg,err) { 
				//alert( msg + ', ' + err);
				
				$('.progressBackgroundFilter').attr('style', 'display:none');
					$('#lblERROR').attr('style', 'color:#b83f3f;display:block');
				if(xhr.status == 400){
					var resp =JSON.parse(xhr.responseText);
					$('#lblERROR').html(resp.message);
				}else{
					$('#lblERROR').html('An unexpected error occured.');
				}
                return;
            }   

        });

    }

    else {
        $('.progressBackgroundFilter').attr('style', 'display:none');
        $('#lblERROR').attr('style', 'color: #E44D4D;display:block');
		$('#lblERROR').html('Please enter required fields.');
    }


});