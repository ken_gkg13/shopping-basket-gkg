$( document ).ready(function() {


   var customer = localStorage.getItem("customerId");
   if(customer == undefined){
   	window.location.href = "/login";
   }
   

  $.get("/api/v1/baskets/summary/"+customer, 
  function(data, status){
  	var resp_data =JSON.stringify(data);
  	
  	var basketId = localStorage.getItem("basketId");
  	if(basketId == undefined || basketId == null){
  	 
  		  localStorage.setItem("basketId", data.basketList[0].basketId);
  		  localStorage.setItem("basketName", data.basketList[0].basketName);
  		  localStorage.setItem("numberOfItems", data.basketList[0].numberOfItems);
  		  
  	}
  	
  	basketId = localStorage.getItem("basketId");
  	var basketName = localStorage.getItem("basketName");
  	var numberOfItems = localStorage.getItem("numberOfItems");
  	
  	$("#dropdownBaskets").html(basketName);
	
	var iconAppend = numberOfItems  + ' <i class="fa fa-shopping-basket"></i>';
	var iconUrl = '/view-basket/' + basketId;
	$("#lnkHeaderViewBasket").attr('href',iconUrl);
	$("#lnkHeaderViewBasket").html(iconAppend);

  	for(var x=0;x<data.basketList.length;x++){
  
  
  	    var linkbasketId = data.basketList[x].basketId;
  	    var linkbasketName = data.basketList[x].basketName;
  	    var linkbasketNumberofItems = data.basketList[x].numberOfItems;
  	    
  	 	$("#dropdownBasketsList").append('<a data-id="' + linkbasketId +'"  data-name="' +  linkbasketName + '" data-num="' + linkbasketNumberofItems + '" class="dropdown-item lnkbasketdrop" href="#">'+data.basketList[x].basketName+'</a>');
		

  	}
	  $(".lnkbasketdrop").bind('click', function (e) {
		   var basket_id =$( this ).attr('data-id');
		   var basket_name =$( this ).attr('data-name');
		   var basket_num =$( this ).attr('data-num');
		
  		  localStorage.setItem("basketId", basket_id);
  		  localStorage.setItem("basketName", basket_name);
  		  localStorage.setItem("numberOfItems", basket_num);
  		  
  		  	$("#dropdownBaskets").html(basket_name);
	
			var iconAppend = basket_num  + ' <i class="fa fa-shopping-basket"></i>';
			var iconUrl = '/view-basket/' + basket_id;
			$("#lnkHeaderViewBasket").attr('href',iconUrl);
			$("#lnkHeaderViewBasket").html(iconAppend);
	});

  });
   
   
   
   $(".btn-add-item").click(function(){
   		var itemName = $(this).attr('data-name');
   		$('.progressBackgroundFilter').attr('style', 'display:block');
   		basketId = localStorage.getItem("basketId");
   		var obj =JSON.stringify({
            itemName: itemName
        })
   		 $.ajax({
            type: "POST",
            url: "/api/v1/baskets/addToBasket/" + basketId,
            data: obj,
            contentType: "application/json",
        	dataType: "json",
            success: function (data) {
                if (data == 'false') {
                    return;
                }
                else {
            		numItems = localStorage.getItem("numberOfItems");
            		numItems = parseInt(numItems) +1;
            		localStorage.setItem("numberOfItems",numItems);
            		alert('Successfully added to basket!');
                    window.location.href = "/shop";
                    return;
                }

            },
            error: function(xhr,msg,err) { 
				//alert( msg + ', ' + err);
				
				$('.progressBackgroundFilter').attr('style', 'display:none');
					$('#lblERROR').attr('style', 'color:#b83f3f;display:block');
				if(xhr.status == 400){
					var resp =JSON.parse(xhr.responseText);
					$('#lblERROR').html(resp.message);
				}else{
					$('#lblERROR').html('An unexpected error occured.');
				}
                return;
            }   

        });
   		
   		
   });
   
   $(".btn-remove-item").click(function(){
   		var itemName = $(this).attr('data-name');
   		$('.progressBackgroundFilter').attr('style', 'display:block');
   		basketId = localStorage.getItem("basketId");
   		var obj =JSON.stringify({
            itemName: itemName
        })
   		 $.ajax({
            type: "POST",
            url: "/api/v1/baskets/removeToBasket/" + basketId,
            data: obj,
            contentType: "application/json",
        	dataType: "json",
            success: function (data) {
                if (data == 'false') {
                    return;
                }
                else {
            		numItems = localStorage.getItem("numberOfItems");
            		numItems = parseInt(numItems) -1;
            		localStorage.setItem("numberOfItems",numItems);
            		alert('Item removed to basket!');
                    window.location.href = "/shop";
                    return;
                }

            },
            error: function(xhr,msg,err) { 
				//alert( msg + ', ' + err);
				
				$('.progressBackgroundFilter').attr('style', 'display:none');
					$('#lblERROR').attr('style', 'color:#b83f3f;display:block');
				if(xhr.status == 400){
					var resp =JSON.parse(xhr.responseText);
					alert(resp.message);
				}else{
					var resp =JSON.parse(xhr.responseText);
					alert(resp.message);
				}
                return;
            }   

        });

   });
});
