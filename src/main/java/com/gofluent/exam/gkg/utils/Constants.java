package com.gofluent.exam.gkg.utils;

public class Constants {

	//[Date Patterns]
	public static final String DATE_PATTERN = "yyyy-MM-dd";
	public static final String DATETIME_PATTERN = "yyyy-MM-dd HH:mm:ss";
	
	//[Message Returns]
	public static final String CREATED = "Created successfully.";
	public static final String UPDATED = "Updated successfully.";
	public static final String DELETED = "Deleted successfully.";
	public static final String NOT_FOUND = "No data found.";
	public static final String EXISTING = "Data already Exists.";
	public static final String STATUS_ACTIVE = "Active";

}
