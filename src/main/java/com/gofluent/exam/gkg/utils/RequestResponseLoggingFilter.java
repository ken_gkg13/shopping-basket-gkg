package com.gofluent.exam.gkg.utils;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Collection;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ReadListener;
import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.ServletOutputStream;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.WriteListener;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.output.TeeOutputStream;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class RequestResponseLoggingFilter implements Filter {

	private final Logger LOGGER = LoggerFactory.getLogger(getClass());

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest httpRequest = (HttpServletRequest) request;
		HttpServletResponse httpResponse = (HttpServletResponse) response;
		BufferedRequestWrapper requestWrapper = null;
		BufferedResponseWrapper responseWrapper = null;
		responseWrapper = new BufferedResponseWrapper(httpResponse);

		StringBuilder message = new StringBuilder("REQUEST: [");
		message.append("method=").append(httpRequest.getMethod());
		message.append(", url=").append(httpRequest.getRequestURI());
		Map<String, String[]> parameterMap = httpRequest.getParameterMap();

		if (!parameterMap.isEmpty()) {
			message.append(", parameters=[");

			Iterator<String> pIt = parameterMap.keySet().iterator();

			for (int i = 0; pIt.hasNext(); i++) {
				String key = pIt.next();
				String[] value = parameterMap.get(key);
				message.append(key).append("=");

				if (value.length > 1)
					message.append(Arrays.deepToString(value));
				else
					message.append(value[0]);

				if (i < parameterMap.size() - 1)
					message.append(", ");
			}
			message.append("]");
		}
		message.append(", headers=[");
		Map<String, String> headersMap = new LinkedHashMap<>();
		Enumeration<String> headers = httpRequest.getHeaderNames();

		while (headers.hasMoreElements()) {
			String headerName = headers.nextElement();
			headersMap.put(headerName, httpRequest.getHeader(headerName));
		}
		Iterator<String> it = headersMap.keySet().iterator();

		for (int i = 0; it.hasNext(); i++) {
			String headerName = it.next();

			if ("cookie".equals(headerName))
				continue;
			message.append(headerName).append("=").append(headersMap.get(headerName));

			if (i < headersMap.size() - 1)
				message.append(", ");
		}
		message.append("]");

		switch (httpRequest.getMethod()) {
		case "POST":
		case "PUT":
		case "PATCH":
			requestWrapper = new BufferedRequestWrapper(httpRequest);
			message.append(", body=").append(requestWrapper.getRequestBody());
			break;
		}
		message.append("]");
		LOGGER.info(message.toString());

		if (requestWrapper != null)
			chain.doFilter(requestWrapper, responseWrapper);
		else
			chain.doFilter(request, responseWrapper);
		StringBuilder responseBuilder = new StringBuilder().append("RESPONSE: [status=")
				.append(responseWrapper.getStatus()).append(", body=").append(responseWrapper.getContent()).append("]");
		LOGGER.info(responseBuilder.toString());
	}

	@Override
	public void destroy() {
	}

	private final class BufferedRequestWrapper extends HttpServletRequestWrapper {

		private ByteArrayInputStream bais;
		private ByteArrayOutputStream baos;
		private BufferedServletInputStream bsis;
		private byte[] buffer;

		public BufferedRequestWrapper(HttpServletRequest request) throws IOException {
			super(request);

			InputStream inputStream = request.getInputStream();
			baos = new ByteArrayOutputStream();
			byte buff[] = new byte[1024];
			int read;

			while ((read = inputStream.read(buff)) > 0)
				this.baos.write(buff, 0, read);
			this.buffer = this.baos.toByteArray();
		}

		@Override
		public ServletInputStream getInputStream() {
			this.bais = new ByteArrayInputStream(this.buffer);
			this.bsis = new BufferedServletInputStream(this.bais);
			return this.bsis;
		}

		public String getRequestBody() throws IOException {
			try (InputStreamReader inputReader = new InputStreamReader(this.getInputStream());
					BufferedReader reader = new BufferedReader(inputReader)) {
				String line = null;
				StringBuilder inputBuilder = new StringBuilder();
				do {
					line = reader.readLine();

					if (StringUtils.isNotEmpty(line))
						inputBuilder.append(line.trim());
				} while (line != null);
				return StringUtils.normalizeSpace(inputBuilder.toString().trim());
			}
		}

	}

	private final class BufferedServletInputStream extends ServletInputStream {

		private ByteArrayInputStream bais;

		public BufferedServletInputStream(ByteArrayInputStream bais) {
			this.bais = bais;
		}

		@Override
		public boolean isFinished() {
			return false;
		}

		@Override
		public boolean isReady() {
			return true;
		}

		@Override
		public void setReadListener(ReadListener listener) {

		}

		@Override
		public int read() throws IOException {
			return bais.read();
		}

		@Override
		public int read(byte[] b, int off, int len) throws IOException {
			return bais.read(b, off, len);
		}

	}

	public class TeeServletOutputStream extends ServletOutputStream {

		private final TeeOutputStream targetStream;

		public TeeServletOutputStream(OutputStream one, OutputStream two) {
			targetStream = new TeeOutputStream(one, two);
		}

		@Override
		public void write(int arg0) throws IOException {
			this.targetStream.write(arg0);
		}

		public void flush() throws IOException {
			super.flush();
			this.targetStream.flush();
		}

		public void close() throws IOException {
			super.close();
			this.targetStream.close();
		}

		@Override
		public boolean isReady() {
			return false;
		}

		@Override
		public void setWriteListener(WriteListener writeListener) {

		}
	}

	public class BufferedResponseWrapper implements HttpServletResponse {

		HttpServletResponse original;
		TeeServletOutputStream tee;
		ByteArrayOutputStream bos;

		public BufferedResponseWrapper(HttpServletResponse response) {
			original = response;
		}

		public String getContent() {
			if (bos != null)
				return bos.toString();
			return null;
		}

		public PrintWriter getWriter() throws IOException {
			return original.getWriter();
		}

		public ServletOutputStream getOutputStream() throws IOException {
			if (tee == null) {
				bos = new ByteArrayOutputStream();
				tee = new TeeServletOutputStream(original.getOutputStream(), bos);
			}
			return tee;

		}

		@Override
		public String getCharacterEncoding() {
			return original.getCharacterEncoding();
		}

		@Override
		public String getContentType() {
			return original.getContentType();
		}

		@Override
		public void setCharacterEncoding(String charset) {
			original.setCharacterEncoding(charset);
		}

		@Override
		public void setContentLength(int len) {
			original.setContentLength(len);
		}

		@Override
		public void setContentLengthLong(long l) {
			original.setContentLengthLong(l);
		}

		@Override
		public void setContentType(String type) {
			original.setContentType(type);
		}

		@Override
		public void setBufferSize(int size) {
			original.setBufferSize(size);
		}

		@Override
		public int getBufferSize() {
			return original.getBufferSize();
		}

		@Override
		public void flushBuffer() throws IOException {
			tee.flush();
		}

		@Override
		public void resetBuffer() {
			original.resetBuffer();
		}

		@Override
		public boolean isCommitted() {
			return original.isCommitted();
		}

		@Override
		public void reset() {
			original.reset();
		}

		@Override
		public void setLocale(Locale loc) {
			original.setLocale(loc);
		}

		@Override
		public Locale getLocale() {
			return original.getLocale();
		}

		@Override
		public void addCookie(Cookie cookie) {
			original.addCookie(cookie);
		}

		@Override
		public boolean containsHeader(String name) {
			return original.containsHeader(name);
		}

		@Override
		public String encodeURL(String url) {
			return original.encodeURL(url);
		}

		@Override
		public String encodeRedirectURL(String url) {
			return original.encodeRedirectURL(url);
		}

		@SuppressWarnings("deprecation")
		@Override
		public String encodeUrl(String url) {
			return original.encodeUrl(url);
		}

		@SuppressWarnings("deprecation")
		@Override
		public String encodeRedirectUrl(String url) {
			return original.encodeRedirectUrl(url);
		}

		@Override
		public void sendError(int sc, String msg) throws IOException {
			original.sendError(sc, msg);
		}

		@Override
		public void sendError(int sc) throws IOException {
			original.sendError(sc);
		}

		@Override
		public void sendRedirect(String location) throws IOException {
			original.sendRedirect(location);
		}

		@Override
		public void setDateHeader(String name, long date) {
			original.setDateHeader(name, date);
		}

		@Override
		public void addDateHeader(String name, long date) {
			original.addDateHeader(name, date);
		}

		@Override
		public void setHeader(String name, String value) {
			original.setHeader(name, value);
		}

		@Override
		public void addHeader(String name, String value) {
			original.addHeader(name, value);
		}

		@Override
		public void setIntHeader(String name, int value) {
			original.setIntHeader(name, value);
		}

		@Override
		public void addIntHeader(String name, int value) {
			original.addIntHeader(name, value);
		}

		@Override
		public void setStatus(int sc) {
			original.setStatus(sc);
		}

		@SuppressWarnings("deprecation")
		@Override
		public void setStatus(int sc, String sm) {
			original.setStatus(sc, sm);
		}

		@Override
		public String getHeader(String arg0) {
			return original.getHeader(arg0);
		}

		@Override
		public Collection<String> getHeaderNames() {
			return original.getHeaderNames();
		}

		@Override
		public Collection<String> getHeaders(String arg0) {
			return original.getHeaders(arg0);
		}

		@Override
		public int getStatus() {
			return original.getStatus();
		}

	}

}