package com.gofluent.exam.gkg.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gofluent.exam.gkg.domain.CatalogEntity;
import com.gofluent.exam.gkg.model.ApiResponse;
import com.gofluent.exam.gkg.model.CatalogRequest;
import com.gofluent.exam.gkg.utils.Constants;
import com.gofluent.exam.services.CatalogService;

import springfox.documentation.annotations.ApiIgnore;

@RestController
@Validated
@RequestMapping("/api/v1/catalog")
public class CatalogController {
	
	@Autowired
	private CatalogService catalogService;
	
	
	@PostMapping("/create")
	public ResponseEntity<Object> createCatalog(@Valid @RequestBody CatalogRequest request) {
		ApiResponse resp = new ApiResponse();
		CatalogEntity checkExisting = catalogService.findOneByItemName(request.getItemName());

		if (checkExisting != null) {
			resp.setMessage(Constants.EXISTING);
			return ResponseEntity.badRequest().body(resp);
		}

		CatalogEntity bskt = catalogService.save(request);
		resp.setMessage(Constants.CREATED);
		resp.setData(bskt);

		return ResponseEntity.status(HttpStatus.CREATED).body(resp);
	}
	
	
	@GetMapping("/findall")
	public ResponseEntity<Object> findAll() {
		return ResponseEntity.ok().body(catalogService.findAll());
	}
	
	@ApiIgnore
	@GetMapping("/find/{category}")
	public ResponseEntity<Object> findbyCategory(@Valid @PathVariable String category) {
		return ResponseEntity.ok().body(catalogService.findByCategory(category));
	}
}
