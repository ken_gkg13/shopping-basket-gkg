package com.gofluent.exam.gkg.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.gofluent.exam.gkg.domain.BasketEntity;
import com.gofluent.exam.gkg.model.BasketSummary;
import com.gofluent.exam.services.BasketItemsService;
import com.gofluent.exam.services.BasketService;
import com.gofluent.exam.services.CatalogService;

@Controller
@RequestMapping("/")
public class DisplayController {

	@Autowired
	private BasketService basketService;
	
	@Autowired
	BasketItemsService basketItemsService;
	
	@Autowired
	private CatalogService catalogService;
	
	@Value("${app.name}")
	String appName;
	
	//Login
	@GetMapping("/login")
	public String loginPage(Model model) {
		return "login";
	}

	@GetMapping("/signup")
	public String signupPage(Model model) {
		return "signup";
	}

	//Baskets
	@GetMapping("/my-baskets")
	public String basketPage(Model model) {
		return "my-baskets";
	}
	
	@GetMapping("/view-basket/{basketId}")
	public String viewBasketPage(Model model, @Valid @PathVariable String basketId) {
		BasketEntity basket = basketService.findOneByBasketId(basketId);
		if (basket == null) {
			return "my-baskets";
		}
		model.addAttribute("basketId", basketId);
		model.addAttribute("basketName", basket.getBasketName());
		
		BasketSummary smry = basketItemsService.findByBasketId(basketId);
		model.addAttribute("basketTotalPrice", smry.getBasketTotalPrice());
		model.addAttribute("basketNumberofItems", smry.getNumberofItems());
		model.addAttribute("basketItems", smry.getItemsList());
		
		return "view-basket";
	}
	
	@GetMapping("/create-basket")
	public String createBasketPage(Model model) {
		return "create-basket";
	}
	
   //Shop
	@GetMapping("/shop")
	public String shopPage(Model model) {
		model.addAttribute("catalogs", catalogService.findAll());
		return "shop";
	}
	

}
