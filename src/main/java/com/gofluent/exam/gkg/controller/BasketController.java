package com.gofluent.exam.gkg.controller;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gofluent.exam.services.BasketService;
import com.gofluent.exam.services.CatalogService;
import com.gofluent.exam.services.BasketItemsService;

import com.gofluent.exam.gkg.domain.BasketEntity;
import com.gofluent.exam.gkg.domain.BasketItemsEntity;
import com.gofluent.exam.gkg.domain.CatalogEntity;
import com.gofluent.exam.gkg.model.AllUsersBasketsSummary;
import com.gofluent.exam.gkg.model.BasketItemsRequest;
import com.gofluent.exam.gkg.model.ApiResponse;
import com.gofluent.exam.gkg.model.BasketRequest;
import com.gofluent.exam.gkg.model.BasketSummary;
import com.gofluent.exam.gkg.utils.Constants;
/**
*
* @author Glen Gonzales
* @version 1.0
* @since 11/17/2020
*
*/

@RestController
@Validated
@RequestMapping("/api/v1/baskets")
public class BasketController {

	@Autowired
	private BasketService basketService;

	@Autowired
	private BasketItemsService basketItemsService;
	
	@Autowired
	private CatalogService catalogService;

	@PostMapping("/create")
	public ResponseEntity<Object> createBasket(@Valid @RequestBody BasketRequest request) {
		ApiResponse resp = new ApiResponse();
		BasketEntity checkExisting = basketService.findOneByUserIdAndBasketName(request.getUserId(),
				request.getBasketName());

		if (checkExisting != null) {
			resp.setMessage(Constants.EXISTING);
			return ResponseEntity.badRequest().body(resp);
		}

		BasketEntity bskt = basketService.save(request);
		resp.setMessage(Constants.CREATED);
		resp.setData(bskt);

		return ResponseEntity.status(HttpStatus.CREATED).body(resp);
	}

	@GetMapping("/findall")
	public ResponseEntity<Object> findAll() {
		return ResponseEntity.ok().body(basketService.findAll());
	}

	@GetMapping("/find/{userId}")
	public ResponseEntity<Object> findByUserId(@Valid @PathVariable String userId) {
		return ResponseEntity.ok().body(basketService.findByUserId(userId));
	}

	@GetMapping("/find/{basketId}")
	public ResponseEntity<Object> findOneByBasketId(@Valid @PathVariable String basketId) {
		BasketEntity basket = basketService.findOneByBasketId(basketId);
		if (basket == null) {
			ApiResponse resp = new ApiResponse();
			resp.setMessage(Constants.NOT_FOUND);
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(resp);
		}
		return ResponseEntity.ok().body(basket);
	}

	@GetMapping("/find/{userId}/{basketId}")
	public ResponseEntity<Object> findOneByUserIdAndBasketId(@Valid @PathVariable String userId,
			@Valid @PathVariable String basketId) {

		BasketEntity basket = basketService.findOneByUserIdAndBasketId(userId, basketId);

		if (basket == null) {
			ApiResponse resp = new ApiResponse();
			resp.setMessage(Constants.NOT_FOUND);
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(resp);
		}

		return ResponseEntity.ok().body(basket);
	}

	@GetMapping("/summary/{userId}")
	public ResponseEntity<Object> getAllUsersBasketsSummary(@Valid @PathVariable String userId) {

		AllUsersBasketsSummary bskSummary = new AllUsersBasketsSummary();

		bskSummary = basketService.getAllUsersBasketsSummary(userId);

		return ResponseEntity.ok().body(bskSummary);
	}

	@PostMapping("/addToBasket/{basketId}")
	public ResponseEntity<Object> addItemToBasket(@Valid @PathVariable String basketId,
			@Valid @RequestBody BasketItemsRequest request) {
		ApiResponse resp = new ApiResponse();
		BasketEntity basket = basketService.findOneByBasketId(basketId);
		if (basket == null) {
			resp.setMessage(Constants.NOT_FOUND + " Basket not found.");
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(resp);
		}

		CatalogEntity catalog = catalogService.findOneByItemName(request.getItemName());
		if (catalog == null) {
			resp.setMessage(Constants.NOT_FOUND + " Product not found.");
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(resp);
		}
		
		BasketItemsEntity checkExisting = basketItemsService.findByItemNameAndBasketId(request.getItemName(), basketId);
		if (checkExisting != null) {
			checkExisting.setQuantity(checkExisting.getQuantity() + 1);
			checkExisting.setPrice(checkExisting.getPrice() + catalog.getPrice());
			checkExisting.setCatalogId(catalog.getCatalogId());
			basketItemsService.save(checkExisting);
			resp.setMessage(Constants.UPDATED);
			resp.setData(checkExisting);
			return ResponseEntity.status(HttpStatus.OK).body(resp);
		}

		BasketItemsEntity bskt = basketItemsService.save(request, basketId,catalog);
		resp.setMessage(Constants.CREATED);
		resp.setData(bskt);
		return ResponseEntity.status(HttpStatus.CREATED).body(resp);
	}

	@PostMapping("/removeToBasket/{basketId}")
	public ResponseEntity<Object> removeItemToBasket(@Valid @PathVariable String basketId,
			@Valid @RequestBody BasketItemsRequest request) {
		ApiResponse resp = new ApiResponse();
		BasketEntity basket = basketService.findOneByBasketId(basketId);
		if (basket == null) {
			resp.setMessage(Constants.NOT_FOUND + " Basket not found.");
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(resp);
		}
		CatalogEntity catalog = catalogService.findOneByItemName(request.getItemName());
		if (catalog == null) {
			resp.setMessage(Constants.NOT_FOUND + " Product not found.");
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(resp);
		}
		
		BasketItemsEntity checkExisting = basketItemsService.findByItemNameAndBasketId(request.getItemName(), basketId);
		if (checkExisting == null) {
			resp.setMessage(Constants.NOT_FOUND + " Item not found in basket.");
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(resp);
		}

		if (checkExisting.getQuantity() > 1) {
			checkExisting.setQuantity(checkExisting.getQuantity() - 1);
			checkExisting.setPrice(checkExisting.getPrice() - catalog.getPrice());
			basketItemsService.save(checkExisting);
			resp.setMessage(Constants.UPDATED);
			resp.setData(checkExisting);
			return ResponseEntity.status(HttpStatus.OK).body(resp);
		} else {

			basketItemsService.deleteById(checkExisting.getBasketItemId());
			resp.setMessage("Item Deleted.");
			resp.setData(null);
			return ResponseEntity.status(HttpStatus.OK).body(resp);

		}

	}

	@GetMapping("/getItems/{basketId}")
	public ResponseEntity<Object> getItemsByBasket(@Valid @PathVariable String basketId) {

		BasketSummary smry = basketItemsService.findByBasketId(basketId);

		return ResponseEntity.status(HttpStatus.OK).body(smry);
	}

}
