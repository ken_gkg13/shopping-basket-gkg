package com.gofluent.exam.gkg.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gofluent.exam.gkg.domain.UserEntity;
import com.gofluent.exam.gkg.model.ApiResponse;
import com.gofluent.exam.gkg.model.Login;
import com.gofluent.exam.gkg.model.UserModel;
import com.gofluent.exam.gkg.utils.Constants;
import com.gofluent.exam.services.UserService;

@RestController
@Validated
@RequestMapping("/api/v1/users")
public class LoginController {

	@Autowired
	private UserService userService;

	@PostMapping("/create")
	public ResponseEntity<Object> createUser(@Valid @RequestBody UserModel request) {
		ApiResponse resp = new ApiResponse();
		UserEntity checkExisting = userService.findOneByEmail(request.getEmail());

		if (checkExisting != null) {
			resp.setMessage(Constants.EXISTING);
			return ResponseEntity.badRequest().body(resp);
		}
        //did not add encryption, just for demo
		UserEntity usr = userService.save(request);
		resp.setMessage(Constants.CREATED);
		resp.setData(usr);

		return ResponseEntity.status(HttpStatus.CREATED).body(resp);
	}

	@GetMapping("/findAll")
	public ResponseEntity<Object> getAllUser() {
		return ResponseEntity.ok().body(userService.findAll());
	}

	@GetMapping("/find/{userId}")
	public ResponseEntity<Object> getUserById(@Valid @PathVariable String userId) {

		UserEntity user = userService.findOneByUserId(userId);
		if (user == null) {
			ApiResponse resp = new ApiResponse();
			resp.setMessage(Constants.NOT_FOUND);
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(resp);
		}

		return ResponseEntity.ok().body(user);
	}

	@PostMapping("/validateLogin")
	public ResponseEntity<Object> validateLogin(@Valid @RequestBody Login request) {

		UserEntity user = userService.findOneByEmailAndPassword(request.getEmail(), request.getPassword());
		if (user == null) {
			ApiResponse resp = new ApiResponse();
			resp.setMessage(Constants.NOT_FOUND);
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(resp);
		}

		return ResponseEntity.ok().body(user);
	}

}
