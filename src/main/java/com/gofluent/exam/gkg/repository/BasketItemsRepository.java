package com.gofluent.exam.gkg.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

import com.gofluent.exam.gkg.domain.BasketItemsEntity;

public interface BasketItemsRepository extends JpaRepository<BasketItemsEntity, String> {

	BasketItemsEntity findByItemNameAndBasketId(String itemName,String basketId);

	List<BasketItemsEntity> findByBasketId(String basketId);


}
