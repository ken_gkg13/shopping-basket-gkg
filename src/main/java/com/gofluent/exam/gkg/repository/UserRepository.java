package com.gofluent.exam.gkg.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.gofluent.exam.gkg.domain.UserEntity;

public interface UserRepository extends JpaRepository<UserEntity, String> {

	UserEntity findOneByUserId(String userId);

	UserEntity findOneByEmail(String email);

	UserEntity findOneByEmailAndPassword(String email, String password);

}
