package com.gofluent.exam.gkg.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import com.gofluent.exam.gkg.domain.CatalogEntity;

public interface CatalogRepository extends JpaRepository<CatalogEntity, String> {

	CatalogEntity findOneByItemName(String itemName);

	List<CatalogEntity> findByCategory(String category);

}
