package com.gofluent.exam.gkg.repository;

import java.util.List;

import javax.validation.Valid;

import org.springframework.data.jpa.repository.JpaRepository;

import com.gofluent.exam.gkg.domain.BasketEntity;

public interface BasketRepository extends JpaRepository<BasketEntity, String> {

	List<BasketEntity> findByUserId(String userId);

	BasketEntity findOneByUserIdAndBasketId(String userId, String basketId);

	BasketEntity findOneByUserIdAndBasketName(String userId, String basketName);

	BasketEntity findOneByBasketId(@Valid String basketId);

}
