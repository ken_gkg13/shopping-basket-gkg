package com.gofluent.exam.gkg;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("com.gofluent.exam")
public class ShoppingBasketApplication {

	public static void main(String[] args) {
		SpringApplication.run(ShoppingBasketApplication.class, args);
	}

}
