package com.gofluent.exam.gkg.config;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.gofluent.exam.gkg.utils.RequestResponseLoggingFilter;
import com.gofluent.exam.gkg.utils.Slf4jMdcFilter;



@Configuration
public class FilterConfig {

	@Bean
	public FilterRegistrationBean<Slf4jMdcFilter> slf4jMdcFilterConfig() {
		FilterRegistrationBean<Slf4jMdcFilter> registrationBean = new FilterRegistrationBean<>();
		registrationBean.setFilter(new Slf4jMdcFilter());
		registrationBean.setOrder(1);
		registrationBean.addUrlPatterns("/api/*");
		return registrationBean;
	}

	@Bean
	public FilterRegistrationBean<RequestResponseLoggingFilter> requestResponseLoggingFilterConfig() {
		FilterRegistrationBean<RequestResponseLoggingFilter> registrationBean = new FilterRegistrationBean<>();
		registrationBean.setFilter(new RequestResponseLoggingFilter());
		registrationBean.setOrder(2);
		registrationBean.addUrlPatterns("/api/*");
		return registrationBean;
	}

}