package com.gofluent.exam.gkg.config;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import com.google.common.base.Predicates;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

    private static final Set<String> PRODUCES = 
            new HashSet<>(Arrays.asList("application/json"));
    
    private static final Set<String> CONSUMES = 
            new HashSet<>(Arrays.asList("application/json"));

    @Bean
    public Docket docket() {
        return new Docket(DocumentationType.SWAGGER_2)
            .produces(PRODUCES)
            .consumes(CONSUMES)
            .select()
            .paths(PathSelectors.any())
            .paths(Predicates.not(PathSelectors.regex("/error")))
            .apis(RequestHandlerSelectors.basePackage("com.gofluent.exam.gkg.controller"))
            .build();
    }

}