package com.gofluent.exam.gkg.domain;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.gofluent.exam.gkg.utils.Constants;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "TBL_USER")
@ApiModel
@Getter
@Setter
public class UserEntity {
	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid2")
	@ApiModelProperty(readOnly = true)
	private String userId;

	@Column(nullable = false)
	private String email;
	
	@Column(nullable = false)
	private String password;
	
	@Column(nullable = false)
	private String firstName;

	@Column(nullable = false)
	private String lastName;

	@JsonFormat(pattern = Constants.DATETIME_PATTERN)
	@CreationTimestamp
	@Column(nullable = false)
	private LocalDateTime dateCreated;

	@JsonFormat(pattern = Constants.DATETIME_PATTERN)
	@UpdateTimestamp
	private LocalDateTime dateModified;

	@Column(nullable = false)
	private String status;
}
