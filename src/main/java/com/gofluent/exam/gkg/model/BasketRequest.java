package com.gofluent.exam.gkg.model;

import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BasketRequest {

	@NotNull
	private String basketName;
	
	private String basketDescription;

	@NotNull
	private String userId;
	
	@NotNull
	private String status;
}
