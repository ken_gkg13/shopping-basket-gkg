package com.gofluent.exam.gkg.model;

import java.time.LocalDateTime;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.gofluent.exam.gkg.utils.Constants;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ApiResponse {
	private String message;

	@JsonFormat(pattern = Constants.DATETIME_PATTERN)
	private LocalDateTime timestamp = LocalDateTime.now();
	private Object data;

}
