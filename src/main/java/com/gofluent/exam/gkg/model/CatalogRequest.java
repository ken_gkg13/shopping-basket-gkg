package com.gofluent.exam.gkg.model;

import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CatalogRequest {

	@NotNull
	private String itemName;
	
	@NotNull
	private Double price;
	
	@NotNull
	private String status;
}
