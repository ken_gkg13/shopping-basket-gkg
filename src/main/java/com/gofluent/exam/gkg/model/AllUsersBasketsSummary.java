package com.gofluent.exam.gkg.model;

import java.util.List;

import com.gofluent.exam.gkg.domain.BasketEntity;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AllUsersBasketsSummary {
	List<BasketEntity> basketList;
	Double basketTotalPrice;
	Integer numberofBaskets;
	String userId;
}
