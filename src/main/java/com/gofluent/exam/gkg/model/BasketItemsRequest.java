package com.gofluent.exam.gkg.model;

import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BasketItemsRequest {
	
	@NotNull
	private String itemName;
}
