package com.gofluent.exam.gkg.model;

import java.util.List;
import com.gofluent.exam.gkg.domain.BasketItemsEntity;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BasketSummary {
	
	List<BasketItemsEntity> ItemsList;
	String basketId;
	Double basketTotalPrice;
	Integer numberofItems;
}
