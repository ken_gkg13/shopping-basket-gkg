package com.gofluent.exam.services;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import com.gofluent.exam.gkg.domain.BasketItemsEntity;
import com.gofluent.exam.gkg.domain.CatalogEntity;
import com.gofluent.exam.gkg.model.BasketItemsRequest;
import com.gofluent.exam.gkg.model.BasketSummary;
import com.gofluent.exam.gkg.repository.BasketItemsRepository;
import com.gofluent.exam.gkg.utils.Constants;
import com.google.gson.Gson;

@Service
public class BasketItemsService {
	@Autowired
	private BasketItemsRepository basketItemsRepository;
	
	Gson gson = new Gson();
	
	@Transactional(isolation = Isolation.READ_COMMITTED)
	public BasketItemsEntity save(BasketItemsRequest newBasketItem,String basketId,CatalogEntity catalog) {
		BasketItemsEntity bsktEntity = new BasketItemsEntity();
		bsktEntity =gson.fromJson(gson.toJson(newBasketItem), BasketItemsEntity.class);
		bsktEntity.setBasketId(basketId);
		bsktEntity.setStatus(Constants.STATUS_ACTIVE);
		bsktEntity.setQuantity(1);
		bsktEntity.setCatalogId(catalog.getCatalogId());
		bsktEntity.setPrice(catalog.getPrice());
		return basketItemsRepository.save(bsktEntity);
	}
	
	@Transactional(isolation = Isolation.READ_COMMITTED)
	public BasketItemsEntity save(BasketItemsEntity newBasketItem) {
		return basketItemsRepository.save(newBasketItem);
	}
	
	public List<BasketItemsEntity> findAll() {
		return basketItemsRepository.findAll();
	}
	
	public BasketItemsEntity findByItemNameAndBasketId(String itemName, String basketid) {
		return basketItemsRepository.findByItemNameAndBasketId(itemName, basketid);
	}


	public void deleteById(String basketItemId) {
		basketItemsRepository.deleteById(basketItemId);
	}

	public BasketSummary findByBasketId(@Valid String basketId) {
		
		List<BasketItemsEntity> basketItems = basketItemsRepository.findByBasketId(basketId);
		BasketSummary smry = new BasketSummary();
		smry.setBasketId(basketId);
		smry.setItemsList(basketItems);
		smry.setNumberofItems(basketItems.size());
		
		Double basketTotalPrice= 0.00;
		Integer basketTotalItems= 0;
		if(basketItems.size() !=0) {
			for(BasketItemsEntity ent :basketItems) {
				if(ent.getPrice() != null)
					basketTotalPrice = basketTotalPrice + ent.getPrice();
				
				if(ent.getQuantity() != null)
					basketTotalItems = basketTotalItems+ ent.getQuantity();
			}
		}
		smry.setNumberofItems(basketTotalItems);
		smry.setBasketTotalPrice(basketTotalPrice);
		
		return smry;
	}
}
