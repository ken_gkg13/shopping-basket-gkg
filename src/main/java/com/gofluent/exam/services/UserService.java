package com.gofluent.exam.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import com.gofluent.exam.gkg.domain.UserEntity;
import com.gofluent.exam.gkg.model.UserModel;
import com.gofluent.exam.gkg.repository.UserRepository;
import com.google.gson.Gson;

@Service
public class UserService {

	@Autowired
	private UserRepository userRepository;

	Gson gson = new Gson();

	@Transactional(isolation = Isolation.READ_COMMITTED)
	public UserEntity save(UserModel newUser) {
		UserEntity usrEntity = gson.fromJson(gson.toJson(newUser), UserEntity.class);
		return userRepository.save(usrEntity);
	}
	
	@Transactional(isolation = Isolation.READ_COMMITTED)
	public UserEntity save(UserEntity newUser) {
		return userRepository.save(newUser);
	}

	public List<UserEntity> findAll() {
		return userRepository.findAll();
	}

	public UserEntity findOneByUserId(String userId) {
		return userRepository.findOneByUserId(userId);
	}
	public UserEntity findOneByEmail(String email) {
		return userRepository.findOneByEmail(email);
	}
	
	public UserEntity findOneByEmailAndPassword(String email,String password) {
		return userRepository.findOneByEmailAndPassword(email,password);
	}

}
