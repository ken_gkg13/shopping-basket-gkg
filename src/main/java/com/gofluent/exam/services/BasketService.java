package com.gofluent.exam.services;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import com.gofluent.exam.gkg.domain.BasketEntity;
import com.gofluent.exam.gkg.model.AllUsersBasketsSummary;
import com.gofluent.exam.gkg.model.BasketRequest;
import com.gofluent.exam.gkg.repository.BasketRepository;
import com.google.gson.Gson;



@Service
public class BasketService {
	@Autowired
	private BasketRepository basketRepository;
	
	@PersistenceContext
	private EntityManager entityManager;
	
	Gson gson = new Gson();
	
	@Transactional(isolation = Isolation.READ_COMMITTED)
	public BasketEntity save(BasketRequest newBasket) {
		BasketEntity bsktEntity = gson.fromJson(gson.toJson(newBasket), BasketEntity.class);
		return basketRepository.save(bsktEntity);
	}
	
	public List<BasketEntity> findAll() {
		return basketRepository.findAll();
	}
	
	public List<BasketEntity> findByUserId(String userId) {
		return basketRepository.findByUserId(userId);
	}
	
	public BasketEntity findOneByUserIdAndBasketId(String userId, String basketId) {
		return basketRepository.findOneByUserIdAndBasketId(userId,basketId);
	}
	
	public AllUsersBasketsSummary getAllUsersBasketsSummary(String userId) {
		AllUsersBasketsSummary bsktSummary = new AllUsersBasketsSummary();
		
		bsktSummary.setUserId(userId);
		//this should be optimized with stored procedure, just did this approach for basic crud functions
		Query q = entityManager.createNativeQuery(
				"SELECT a.basket_id, a.basket_name, a.basket_description, a.user_id, a.date_created, a.date_modified, a.status, "
				+ "COALESCE((SELECT SUM(b.price) FROM tbl_basket_items b WHERE b.basket_id = a.basket_id),0) as basket_total_price, "
				+ "COALESCE((SELECT SUM(b.quantity) FROM tbl_basket_items b WHERE b.basket_id = a.basket_id),0) as number_of_items  "
				+ "FROM  tbl_basket a WHERE user_id = '"+userId+"'",
				BasketEntity.class);		
		@SuppressWarnings("unchecked")
		List<BasketEntity> bskEntity = q.getResultList();
		
		bsktSummary.setBasketList(bskEntity);
		int numberofBaskets = bskEntity.size();
		
		if(numberofBaskets!=0) {
			Double basketTotalPrice = 0.00; 
			for (BasketEntity i_bskt : bskEntity)
				basketTotalPrice = basketTotalPrice + i_bskt.getBasketTotalPrice();
    
			bsktSummary.setBasketTotalPrice(basketTotalPrice);
			bsktSummary.setNumberofBaskets(numberofBaskets);
		}else {
			bsktSummary.setBasketTotalPrice(0.00);
			bsktSummary.setNumberofBaskets(0);
		}
		return bsktSummary;
	}

	public BasketEntity findOneByUserIdAndBasketName(String userId, String basketName) {
		return basketRepository.findOneByUserIdAndBasketName(userId,basketName);
	}

	public BasketEntity findOneByBasketId(@Valid String basketId) {
		return basketRepository.findOneByBasketId(basketId);
	}
}
