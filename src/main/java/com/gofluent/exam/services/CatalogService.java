package com.gofluent.exam.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import com.gofluent.exam.gkg.domain.CatalogEntity;
import com.gofluent.exam.gkg.model.CatalogRequest;
import com.gofluent.exam.gkg.repository.CatalogRepository;
import com.google.gson.Gson;

@Service
public class CatalogService {
	@Autowired
	private CatalogRepository catalogRepository;

	Gson gson = new Gson();
	
	@Transactional(isolation = Isolation.READ_COMMITTED)
	public CatalogEntity save(CatalogRequest newCatalog) {
		CatalogEntity ctlgEntity = gson.fromJson(gson.toJson(newCatalog), CatalogEntity.class);
		return catalogRepository.save(ctlgEntity);
	}
	
	public List<CatalogEntity> findAll() {
		return catalogRepository.findAll();
	}

	public CatalogEntity findOneByItemName(String itemName) {
		return catalogRepository.findOneByItemName(itemName);
	}
	
	
	public List<CatalogEntity> findByCategory(String category) {
		return catalogRepository.findByCategory(category);
	}
}
